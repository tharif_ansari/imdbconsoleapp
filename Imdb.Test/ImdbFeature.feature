﻿Feature: Imdb
	A system to add or delete a movies

@mytag
Scenario: Add a movie
	Given the Name as "Tenet"
	And the Year as "2020"
	And the Plot as "Tenet is a 2020 action-thriller and science fiction film written and directed by Christopher Nolan."
	And the Actor as "1 2"
	And the Producer as "1"
	When the Movie is added
	Then the name year plot should look like this
	| Name      | Year | Plot                                                                                                                                                                            |
	| VTV       | 2010 | A Hindu assistant director, Karthik, falls in love with Jessie, a Christian from a traditional family. Things change when Karthik becomes busy during a forty-day shoot in Goa. |
	| Chernobyl | 2019 | In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world's worst man-made catastrophes.             |
	| Tenet     | 2020 | Tenet is a 2020 action-thriller and science fiction film written and directed by Christopher Nolan. |
	And the actors should look like this
	| Name         | DOB        |
	| Simbu        | 02/03/1983 |
	| Trisha       | 05/04/1983 |
	| Jared Harris | 08/24/1961 |
	| John Washington  | 06/28/1984 |
	| Robert Pattinson | 05/13/1986 |
	And the producer should look like this
	| Name        | DOB        |
	| Vtv Ganesh  | 12/21/1963 |
	| Craig Mazin | 04/08/1971 |
	| Christopher Nolan | 06/30/1970 |


@mytag1
Scenario: List Movie
	Given that movies are present 
	When the movies are listed
	Then the list should look like this
	| Name      | Year | Plot                                                                                                                                                                            |
	| VTV       | 2010 | A Hindu assistant director, Karthik, falls in love with Jessie, a Christian from a traditional family. Things change when Karthik becomes busy during a forty-day shoot in Goa. |
	| Chernobyl | 2019 | In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world's worst man-made catastrophes.             |
	And the actors should look like this
	| Name         | DOB        |
	| Simbu        | 02/03/1983 |
	| Trisha       | 05/04/1983 |
	| Jared Harris | 08/24/1961 |
	And the producer should look like this
	| Name        | DOB        |
	| Vtv Ganesh  | 12/21/1963 |
	| Craig Mazin | 04/08/1971 |
