﻿using Imdb.Classes;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Test
{
    [Binding]
    public class ImdbSteps
    {
        static string _name;
        static string _plot;
        static int _year;
        static Classes.Movie _movie = null;
        static Actions.MovieRepo _movieController = new Actions.MovieRepo();
        static List<Classes.Movie> AllMovies = new List<Classes.Movie>();
        
        [BeforeFeature]
        public static void AddMovies()
        {
            _name = "VTV";
            _year = 2010;
            _plot = "A Hindu assistant director, Karthik, falls in love with Jessie, a Christian from a traditional family. Things change when Karthik becomes busy during a forty-day shoot in Goa.";
            _movie = new Classes.Movie(_name, _year, _plot);
            _movie.AddActor(new Classes.Actor("Simbu", new DateTime(1983, 02, 03)));
            _movie.AddActor(new Classes.Actor("Trisha", new DateTime(1983, 05, 04)));
            _movie.AddProducer(new Classes.Producer("Vtv Ganesh", new DateTime(1963, 12, 21)));
            _movieController.AddMovie(_movie);

            _name = "Chernobyl";
            _year = 2019;
            _plot = "In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world's worst man-made catastrophes.";
            _movie = new Classes.Movie(_name, _year, _plot);
            _movie.AddActor(new Classes.Actor("Jared Harris", new DateTime(1961, 08, 24)));
            _movie.AddProducer(new Classes.Producer("Craig Mazin", new DateTime(1971, 4, 8)));
            _movieController.AddMovie(_movie);

            
        }
        [Given(@"the Name as ""(.*)""")]
        public void GivenTheNameAs(string p0)
        {
            _name = p0;
        }
        
        [Given(@"the Year as ""(.*)""")]
        public void GivenTheYearAs(int p0)
        {
            _year = p0;
        }
        
        [Given(@"the Plot as ""(.*)""")]
        public void GivenThePlotAs(string p0)
        {
            _plot = p0;
        }
        
        [Given(@"the Actor as ""(.*)""")]
        public void GivenTheActorAs(string p0)
        {
            //this.AddMovies();
            _movie = new Classes.Movie(_name, _year, _plot);
            _movie.AddActor(new Classes.Actor("John Washington", new DateTime(1984, 06, 28)));
            _movie.AddActor(new Classes.Actor("Robert Pattinson", new DateTime(1986, 05, 13)));
        }
        
        [Given(@"the Producer as ""(.*)""")]
        public void GivenTheProducerAs(int p0)
        {
            _movie.AddProducer(new Classes.Producer("Christopher Nolan", new DateTime(1970, 06, 30)));
        }
        
        [When(@"the Movie is added")]
        public void WhenTheMovieIsAdded()
        {
            _movieController.AddMovie(_movie);

            
        }
        
        [Then(@"the name year plot should look like this")]
        public void ThenTheNameYearPlotShouldLookLikeThis(Table table)
        {
            AllMovies = _movieController.GetMovies();
            table.CompareToSet(AllMovies);
            //table.CompareToInstance(AllMovies[0]);
        }
        
        [Then(@"the actors should look like this")]
        public void ThenTheActorsShouldLookLikeThis(Table table)
        {
            var ActorsList = new List<Classes.Actor>();
            foreach (var mov in AllMovies)
                foreach (var actr in mov.Actors)
                    ActorsList.Add(actr);
            table.CompareToSet(ActorsList);
        }
        
        [Then(@"the producer should look like this")]
        public void ThenTheProducerShouldLookLikeThis(Table table)
        {
            var ProducersList = new List<Classes.Producer>();
            foreach (var mov in AllMovies)
                ProducersList.Add(mov.Producers);
            table.CompareToSet(ProducersList);
        }


        [Given(@"that movies are present")]
        public void GivenThatMoviesArePresent()
        {
           
        }

        [When(@"the movies are listed")]
        public void WhenTheMoviesAreListed()
        {
            
        }

        [Then(@"the list should look like this")]
        public void ThenTheListShouldLookLikeThis(Table table)
        {
            AllMovies = _movieController.GetMovies();
            table.CompareToSet(AllMovies);
        }
    }
}
