﻿using Imdb.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Actions
{
    public class ProducerRepo
    {
        public List<Producer> Producers = new List<Producer>();

        public ProducerRepo()
        {
        }

        public void DisplayProducers()
        {
            var Selection = 0;
            foreach (var producer in Producers)
                Console.WriteLine((++Selection).ToString() + " " + producer);
        }
        public int ProducersCount()
        {
            return Producers.Count;
        }
        public Producer GetProducerOnId(int id)
        {
            return Producers[id];
        }
        public void AddProducer(string name, DateTime dob)
        {
            Producers.Add(new Producer(name, dob));
        }
    }
}
