﻿using Imdb.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Actions
{
    public class MovieRepo
    {
        public List<Movie> Movies = new List<Movie>();

        public MovieRepo()
        {

        }

        public void DisplayMovie()
        {
            var Selection = 0;
            foreach (var movie in Movies)
            {
                Console.WriteLine((++Selection).ToString() + ".\n" + movie.ToString() + "\n");

            }
        }
        public List<Movie> GetMovies()
        {
            return Movies;
        }

        public int MoviesCount()
        {
            return Movies.Count;
        }
        public void AddMovie(Movie movie)
        {
            Movies.Add(movie);
        }
        public void DisplayMovieName()
        {
            foreach (var movie in Movies)
                Console.WriteLine(movie.GetMovieName());
        }
        public bool DeleteMovie(int number)
        {
            if (number > 0 && number <= Movies.Count)
            {
                Movies.RemoveAt(number);
                return true;
            }
            return false;
        }
    }
}
