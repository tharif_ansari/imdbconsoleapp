﻿using Imdb.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Actions
{
    public class ActorRepo
    {
        public List<Actor> Actors = new List<Actor>();
        public ActorRepo()
        {
        }
        public void DisplayActors()
        {
            var Selection = 0;
            foreach (var actor in Actors)
                Console.WriteLine((++Selection).ToString() + " " + actor);
        }
        public int ActorsCount()
        {
            return Actors.Count;
        }
        public Actor GetActorOnId(int id)
        {
            return Actors[id];
        }
        public void AddActor(string name, DateTime dob)
        {
            Actors.Add(new Actor(name, dob));
        }
    }
}
