﻿using Imdb.Enums;
using System;

namespace Imdb
{
    class Program
    {
        static void Main(string[] args)
        {
            IMDBService Controller = new IMDBService();
            for (; ; )
            {
                Console.WriteLine("Enter\n1. List all movies" +
                    "\n2. Add a movie" +
                    "\n3. Add a actor" +
                    "\n4. Add a producer" +
                    "\n5. Delete a movie" +
                    "\n6. Exit the console app\n");
                int UserInput = Convert.ToInt32(Console.ReadLine());
                Selection UserInputEnum = (Selection)UserInput;

                switch (UserInputEnum)
                {

                    case Selection.ListMovies:
                        {
                            Controller.ListMovies();
                            break;
                        }
                    case Selection.AddMovie:
                        {
                            Controller.AddMovie();
                            break;
                        }
                    case Selection.AddActor:
                        {
                            Controller.AddActor();
                            break;
                        }
                    case Selection.AddProducer:
                        {
                            Controller.AddProducer();
                            break;
                        }
                    case Selection.DeleteMovie:
                        {
                            Controller.DeleteMovie();
                            break;
                        }
                    case Selection.Exit:
                        {
                            Console.WriteLine("\nYou chose to exit...\nThank you...");
                            Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Enter a valid Input");
                            break;
                        }
                }
            }
        }
    }
}
