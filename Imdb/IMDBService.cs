﻿using Imdb.Actions;
using Imdb.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb
{
    class IMDBService
    {
        public MovieRepo MovieController = new MovieRepo();
        public ProducerRepo ProducerController = new ProducerRepo();
        public ActorRepo ActorController = new ActorRepo();
        public IMDBService()
        {
        }
        public List<Movie> ret()
        {
            return MovieController.ReturnMovies();
        }
        public void ListMovies()
        {
            if (MovieController.MoviesCount() == 0)
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Movie List is empty..." +
                        "\nAdd a movie to List all movies...\n");
                return;
            }
            Console.WriteLine();
            MovieController.DisplayMovie();
            return;
        }

        public void AddMovie()
        {
            int ActorCount = ActorController.ActorsCount();
            int ProducerCount = ProducerController.ProducersCount();
            if (ActorCount == 0)
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("No Actors present to add a movie...\nPlease" +
                    " add actors to add movie...\n");
                return;
            }


            if (ProducerCount == 0)
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("No Producers present to add a movie...\nPlease" +
                    " add Producers to add movie...\n");
                return;
            }
            Console.WriteLine("\nEnter Movie's Name");
            string Name = Console.ReadLine();

            if (String.IsNullOrWhiteSpace(Name))
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Name cannot be null or whitespace\n");
                return;
            }


            Console.WriteLine("Enter movie year (YYYY)");
            int Year = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter a brief plot of the movie");
            string Plot = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(Plot))
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Plot cannot be null or whitespace\n");
                return;
            }

            Console.WriteLine("Enter the space seperated " +
                "number to add the respective actor");
            ActorController.DisplayActors();

            string ActorsSelection = Console.ReadLine();

            var ActorsSelected = Array.ConvertAll<string, int>
                (ActorsSelection.Split(' '), Convert.ToInt32);

            foreach (var id in ActorsSelected)
            {
                if (id > 0 && id <= ActorCount)
                    continue;
                else
                {
                    Console.WriteLine("*** ERROR ***");
                    Console.WriteLine("Enter Valid Actor Number\n");
                    return;
                }

            }
            Console.WriteLine("Enter the number to add the respective producer");
            ProducerController.DisplayProducers();
            int ProducerNumber = Convert.ToInt32(Console.ReadLine());
            if (ProducerNumber == 0 || ProducerNumber > ProducerCount)
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Enter Valid Producer Number\n");
                return;
            }
            // can i use movie class here
            var movie = new Movie(Name, Year, Plot);
            foreach (var i in ActorsSelected)
                movie.AddActor(ActorController.GetActorOnId(i - 1));
            movie.AddProducer(ProducerController.GetProducerOnId(ProducerNumber - 1));

            MovieController.AddMovie(movie);
            Console.WriteLine();
            return;

        }

        public void AddActor()
        {
            Console.WriteLine("\nEnter the Actor's name");
            string Name = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(Name))
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Name cannot be null or whitespace\n");
                return;
            }
            Console.WriteLine("Enter DOB in MM/DD/YYYY");
            DateTime DOB;
            if (DateTime.TryParse(Console.ReadLine(), out DOB))
            {
                ActorController.AddActor(Name, DOB);
                Console.WriteLine();
                return;
            }
            else
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Please enter a valid date\n");
                return;
            }

        }

        public void AddProducer()
        {
            Console.WriteLine("\nEnter the Producer's name");
            string Name = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(Name))
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Enter a Valid name\n");
                return;
            }

            Console.WriteLine("Enter DOB in MM/DD/YYYY");
            DateTime DOB;
            if (DateTime.TryParse(Console.ReadLine(), out DOB))
            {
                ProducerController.AddProducer(Name, DOB);
                Console.WriteLine();
                return;
            }
            else
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Please enter a valid date\n");
                return;
            }

        }

        public void DeleteMovie()
        {
            var MovieCount = MovieController.MoviesCount();
            if (MovieCount == 0)
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("No movies to delete...\n");
                return;
            }

            Console.WriteLine("Enter the number to delete the respective Movie");
            MovieController.DisplayMovie();
            var Selection = Convert.ToInt32(Console.ReadLine());
            if (Selection < 0 || Selection > MovieCount)
            {
                Console.WriteLine("*** ERROR ***");
                Console.WriteLine("Enter a valid movie number\n");
                return;
            }
            MovieController.DeleteMovie(Selection);
            Console.WriteLine();
            return;
        }
    }
}
