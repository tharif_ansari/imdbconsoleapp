﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Enums
{
    public enum Selection
    {
        ListMovies = 1,
        AddMovie,
        AddActor,
        AddProducer,
        DeleteMovie,
        Exit
    }
    
}
