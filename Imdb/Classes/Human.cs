﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Classes
{
    public class Human
    {
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public Human(string name, DateTime dob)
        {
            Name = name;
            DOB = dob;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
