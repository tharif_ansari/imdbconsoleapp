﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Classes
{
    public class Movie
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; } = new List<Actor>();
        public Producer Producers { get; set; } = null;

        public string GetMovieName()
        {
            return Name;
        }
        public Movie(string name, int year, string plot)
        {
            Name = name;
            Year = year;
            Plot = plot;
        }

        public void AddActor(Actor actor)
        {
            Actors.Add(actor);
        }

        public void AddProducer(Producer producer)
        {
            Producers = producer;
        }

        public override string ToString()
        {
            string ReturnString = Name + " (" + Year.ToString() + ")\n";
            ReturnString += "\nPlot - " + Plot;
            ReturnString += "\nActors - ";
            foreach (var actor in Actors)
                ReturnString += actor.ToString() + "      ";
            ReturnString += "\nProducer - " + Producers;
            return ReturnString;
        }
    }
}
